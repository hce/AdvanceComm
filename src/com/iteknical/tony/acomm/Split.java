package com.iteknical.tony.acomm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;

public class Split {
    /** 分拆时读BUFFER大小 */
    private static final int READ_BUFFER_SIZE   = 8 * 1024;
    /** 分拆时每个输出的句柄BUFFER大小 */
    private static final int OUTPUT_BUFFER_SIZE = 8 * 1024;

    /**
     * 2线程分拆x文件和y文件
     * 
     * @param fileXName
     * @param fileYName
     * @param tempDir
     * @param splitXPrefix 分拆的文件
     * @param splitYPrefix
     * @param splitSum
     * @param charset
     * @throws Exception
     */
    public static void twoThreadsSplit(String fileXName, String fileYName, String tempDir, String splitXPrefix,
        String splitYPrefix, int splitSum, Charset charset) throws Exception {

        final CountDownLatch latch = new CountDownLatch(2);

        Thread splitAThread = buildNewSplitThread(fileXName, tempDir, splitXPrefix, splitSum, charset, latch);
        Thread splitBThread = buildNewSplitThread(fileYName, tempDir, splitYPrefix, splitSum, charset, latch);

        splitAThread.start();
        splitBThread.start();

        latch.await();
    }

    /**
     * 分拆文件
     * 
     * @param fileXName
     * @param tempDir
     * @param splitXPrefix
     * @param splitSum
     * @param charset
     * @throws Exception
     */
    public static void split(String fileXName, String tempDir, String splitXPrefix, int splitSum, Charset charset)
        throws Exception {

        final CountDownLatch latch = new CountDownLatch(1);

        Thread splitAThread = buildNewSplitThread(fileXName, tempDir, splitXPrefix, splitSum, charset, latch);

        splitAThread.start();

        latch.await();
    }

    /**
     * 
     * @param fileName
     * @param tempDir
     * @param splitXPrefix
     * @param splitSum
     * @param charset
     * @param latch
     * @return
     */
    private static Thread buildNewSplitThread(final String fileName, final String tempDir, final String splitXPrefix,
        final int splitSum, final Charset charset,
        final CountDownLatch latch) {
        return new Thread(() -> {
            try {
                System.out.println("split start, file=" + fileName);
                long curTimestamp = System.currentTimeMillis();

                BufferedReader br = new BufferedReader(new FileReader(fileName), READ_BUFFER_SIZE);

                BufferedWriter[] bws = new BufferedWriter[splitSum];
                for (int i = 0; i < splitSum; ++i) {
                    bws[i] = new BufferedWriter(
                        new OutputStreamWriter(
                            new FileOutputStream(
                                new StringBuilder(tempDir).append("/").append(splitXPrefix).append(i).toString()),
                            charset),
                        OUTPUT_BUFFER_SIZE);
                }

                String s;
                while ((s = br.readLine()) != null) {
                    int index = Math.abs(s.hashCode()) % splitSum;
                    bws[index].write(s);
                    bws[index].write('\n');
                }

                br.close();

                for (BufferedWriter bw : bws) {
                    bw.close();
                }

                System.out.println("split finished, file=" + fileName + ", spent="
                    + (System.currentTimeMillis() - curTimestamp) + "ms");
            } catch (Exception e) {
                e.printStackTrace();
            }
            latch.countDown();
        });
    }
}
