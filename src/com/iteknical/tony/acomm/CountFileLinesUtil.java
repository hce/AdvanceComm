package com.iteknical.tony.acomm;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CountFileLinesUtil {

    /** java 8 nio 实现方式1 */
    @Deprecated
    public static final int  COUNT_FILE_LINES_METHOD_NIO_1 = 1;
    /** java io 实现方式1 */
    public static final int  COUNT_FILE_LINES_METHOD_IO_1  = 2;
    /** java io 实现方式2 */
    @Deprecated
    public static final int  COUNT_FILE_LINES_METHOD_IO_2  = 3;
    /** java 8 nio 实现方式1 */
    @Deprecated
    public static final int  COUNT_FILE_LINES_METHOD_NIO_2 = 4;

    /** 行数统计时一次性读byte大小 */
    private static final int BYTE_SIZE                     = 1024 * 8;

    /**
     * 统计文件行数
     * 
     * @param fileName
     * @return
     * @throws Exception
     */
    public static long countFileLines(String fileName) throws Exception {
        return countFileLines(fileName, COUNT_FILE_LINES_METHOD_IO_1);
    }

    /**
     * 统计文件行数
     * 
     * @param fileName
     * @param method
     * @return
     * @throws Exception
     */
    public static long countFileLines(String fileName, int method) throws Exception {
        System.out.println("counting file lines, file=" + fileName);
        long startTime = System.currentTimeMillis();

        long lines = 0L;
        if (method == COUNT_FILE_LINES_METHOD_NIO_1) {
            lines = countFileLinesFromJava8(fileName);
        }
        if (method == COUNT_FILE_LINES_METHOD_IO_1) {
            lines = countFileLinesFromJavaIOMethod1(fileName);
        }
        if (method == COUNT_FILE_LINES_METHOD_IO_2) {
            lines = countFileLinesFromJavaIOMethod2(fileName);
        }
        if (method == COUNT_FILE_LINES_METHOD_NIO_2) {
            lines = countFileLinesFromMemoryMapped(fileName);
        }

        System.out.println("count file lines finish, file=" + fileName + ", lines=" + lines + ", spent="
            + (System.currentTimeMillis() - startTime) + "ms");
        return lines;
    }

    /**
     * java 8 实现的统计行数
     * 
     * @param fileName
     * @return
     * @throws Exception
     */
    private static long countFileLinesFromJava8(String fileName) throws Exception {
        return Files.lines(Paths.get(fileName)).count();
    }

    /**
     * java io 实现1，from https://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
     * 
     * @param fileName
     * @return
     * @throws Exception
     */
    private static long countFileLinesFromJavaIOMethod1(String fileName) throws Exception {
        InputStream is = new BufferedInputStream(new FileInputStream(fileName));
        try {
            byte[] c = new byte[BYTE_SIZE];
            long count = 0L;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            is.close();
        }
    }

    /**
     * java io 实现，from https://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    private static long countFileLinesFromJavaIOMethod2(String fileName) throws Exception {
        InputStream is = new BufferedInputStream(new FileInputStream(fileName));
        try {
            byte[] c = new byte[1024];

            int readChars = is.read(c);
            if (readChars == -1) {
                // bail out if nothing to read
                return 0L;
            }

            // make it easy for the optimizer to tune this loop
            long count = 0;
            while (readChars == 1024) {
                for (int i = 0; i < 1024;) {
                    if (c[i++] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }

            // count remaining characters
            while (readChars != -1) {
                System.out.println(readChars);
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }

            return count == 0 ? 1 : count;
        } finally {
            is.close();
        }
    }

    /**
     * java 内存文件映射实现
     * <p>
     * 不可用，由于inChannel.size()为int，会溢出
     * </p>
     * 
     * @param fileName
     * @return
     * @throws Exception
     */
    private static long countFileLinesFromMemoryMapped(String fileName) throws Exception {
        RandomAccessFile aFile = new RandomAccessFile(fileName, "r");
        FileChannel inChannel = aFile.getChannel();
        MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY,
            0L, inChannel.size());

        buffer.load();

        long count = 0;
        for (int i = 0; i < buffer.limit(); i++) { // There are many strings in the file separated by \n
            if ((char)buffer.get() == '\n') {
                count++;
            }
            // need to make a complete string over here.
        }
        buffer.clear(); // do something with the data and clear/compact it.
        aFile.close();
        return count; // The String which has been made in the above for loop
    }
}
