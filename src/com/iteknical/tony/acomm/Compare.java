package com.iteknical.tony.acomm;

import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class Compare {
    /** 只输出相同的行 */
    public static final int SAME_ONLY = 1;
    /** 合并模式 */
    public static final int COMBINE   = 2;
    /** 差集模式 */
    public static final int DIFFENCE  = 3;

    /**
     * 比较分拆出的小文件，并输出相同部分
     * 
     * @param tempDir
     * @param splitXPrefix
     * @param splitYPrefix
     * @param outputFileName
     * @param splitSum
     * @throws Exception
     */
    public static void singleThreadCompare(String tempDir, String splitXPrefix, String splitYPrefix,
        String outputFileName, int splitSum, int method, Charset charset) throws Exception {
        // System.out.println("compare start");
        // long curTimestamp = System.currentTimeMillis();

        BufferedWriter resultBufferWriter = Files.newBufferedWriter(Paths.get(outputFileName));

        for (int i = 0; i < splitSum; ++i) {
            Set<String> setA = new HashSet<String>();
            Files
                .lines(Paths.get(new StringBuilder(tempDir).append("/").append(splitXPrefix).append(i).toString()),
                    charset)
                .forEach(s -> setA.add(s));

            Set<String> same = new HashSet<String>();
            Set<String> diffence = new HashSet<String>();
            Files
                .lines(Paths.get(new StringBuilder(tempDir).append("/").append(splitYPrefix).append(i).toString()),
                    charset)
                .forEach(s -> {
                    if (setA.contains(s)) {
                        if (method == SAME_ONLY || method == COMBINE) {
                            same.add(s);
                        }
                        if (method == DIFFENCE) {
                            setA.remove(s);
                        }
                    } else {
                        if (method == COMBINE) {
                            diffence.add(s);
                        }
                    }
                });
            if (method == SAME_ONLY || method == COMBINE) {
                for (String s : same) {
                    resultBufferWriter.write(s);
                    resultBufferWriter.write('\n');
                }
            }
            if (method == COMBINE) {
                for (String s : diffence) {
                    resultBufferWriter.write(s);
                    resultBufferWriter.write('\n');
                }
            }
            if (method == DIFFENCE) {
                for (String s : setA) {
                    resultBufferWriter.write(s);
                    resultBufferWriter.write('\n');
                }
            }
        }

        resultBufferWriter.close();

        // System.out.println("compare spent=" + (System.currentTimeMillis() - curTimestamp) + "ms");
    }
}
