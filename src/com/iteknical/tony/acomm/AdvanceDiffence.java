package com.iteknical.tony.acomm;

import java.io.File;
import java.nio.charset.Charset;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;

/**
 * A-B文件差值
 * 
 * @author Tony
 *
 */
public class AdvanceDiffence {

    /** X分拆小文件的前缀 */
    private static final String  SPLIT_X_PREFIX        = "X_";
    /** Y分拆小文件的前缀 */
    private static final String  SPLIT_Y_PREFIX        = "Y_";

    /** 默认的splitNum */
    private static final int     DEFAULT_SPLIT_NUM     = 1024;
    /** 默认的文件编码 */
    private static final Charset DEFAULT_CHARSET       = Charset.forName("UTF-8");

    /** 输入文件A参数 */
    private static final String  FILE_A_NAME_PARA      = "fa";
    /** 输入文件B参数 */
    private static final String  FILE_B_NAME_PARA      = "fb";
    /** 输出文件参数 */
    private static final String  FILE_OUTPUT_NAME_PARA = "o";
    /** 临时文件目录 */
    private static final String  TEMP_DIRECTORY_PARA   = "td";
    /** 临时文件目录 */
    private static final String  CHARSET_PARA          = "c";
    /** 分拆数量 */
    private static final String  SPLIT_SUM_PARA        = "s";
    /** 命令行入参配置 */
    private static Options       options;
    static {
        /** 输入文件A */
        Option fileANameOption = new Option(FILE_A_NAME_PARA, true, "file A");
        /** 输入文件B */
        Option fileBNameOption = new Option(FILE_B_NAME_PARA, true, "file B");
        /** 输出文件 */
        Option outputFileNameOption = new Option(FILE_OUTPUT_NAME_PARA, true, "output file");

        /** 临时文件目录，默认为输入文件A所在目录下TEMP_DIR */
        Option tempDirOption = new Option(TEMP_DIRECTORY_PARA, false, "temp directory");
        /** 文件字符集，默认为UTF-8 */
        Option chatsetOption = new Option(CHARSET_PARA, false, "charset");
        /** 分拆数量，默认为1024 */
        Option splitSumOption = new Option(SPLIT_SUM_PARA, false, "split sum");

        options = new Options();
        options.addOption(fileANameOption);
        options.addOption(fileBNameOption);
        options.addOption(outputFileNameOption);
        options.addOption(tempDirOption);
        options.addOption(chatsetOption);
        options.addOption(splitSumOption);
    }

    public static void main(String[] args) throws Exception {

        CommandLine commandLine = new DefaultParser().parse(options, args);

        if (commandLine.hasOption(FILE_A_NAME_PARA) == false || commandLine.hasOption(FILE_B_NAME_PARA) == false
            || commandLine.hasOption(FILE_OUTPUT_NAME_PARA) == false) {
            System.out.println("parameter missing");
            return;
        }

        /** input file A */
        String fileAName = commandLine.getOptionValue(FILE_A_NAME_PARA);
        if (new File(fileAName).exists() == false) {
            System.out.println("file A not exist");
            return;
        }

        /** input file B */
        String fileBName = commandLine.getOptionValue(FILE_B_NAME_PARA);
        if (new File(fileBName).exists() == false) {
            System.out.println("file B not exist");
            return;
        }

        /** output file */
        String outputFileName = commandLine.getOptionValue(FILE_OUTPUT_NAME_PARA);
        if (new File(outputFileName).exists()) {
            System.out.println("output file exist");
            return;
        }

        /** temp dir */
        String tempDir;
        if (commandLine.hasOption(TEMP_DIRECTORY_PARA)) {
            tempDir = commandLine.getOptionValue(TEMP_DIRECTORY_PARA);
        } else {
            tempDir = new File(fileAName).getParent() + "/TEMP_DIR";
        }
        // 临时目录如果不存在，则创建之
        FileUtils.forceMkdir(new File(tempDir));
        // 清空临时目录文件
        FileUtils.cleanDirectory(new File(tempDir));

        /** 编码 */
        Charset charset;
        if (commandLine.hasOption(CHARSET_PARA)) {
            charset = Charset.forName(commandLine.getOptionValue(CHARSET_PARA));
        } else {
            charset = DEFAULT_CHARSET;
        }

        /** 分拆的数量 */
        int splitNum = DEFAULT_SPLIT_NUM;
        if (commandLine.hasOption(SPLIT_SUM_PARA)) {
            splitNum = Integer.parseInt(commandLine.getOptionValue(SPLIT_SUM_PARA));
        }
        if (splitNum <= 0) {
            System.out.println("split number illegal");
            return;
        }

        long startTime = System.currentTimeMillis();

        System.out.println("[STEP 1] split start");
        long stepStartTime = System.currentTimeMillis();
        Split.twoThreadsSplit(fileAName, fileBName, tempDir, SPLIT_X_PREFIX, SPLIT_Y_PREFIX, splitNum, charset);
        System.out.println("[STEP 1] split finished, spent=" + (System.currentTimeMillis() - stepStartTime) + "ms");

        System.out.println("[STEP 2] compare start");
        stepStartTime = System.currentTimeMillis();
        Compare.singleThreadCompare(tempDir, SPLIT_X_PREFIX, SPLIT_Y_PREFIX, outputFileName, splitNum, Compare.DIFFENCE,
            charset);
        System.out.println("[STEP 2] compare finished, spent=" + (System.currentTimeMillis() - stepStartTime) + "ms");

        // 干完活清空临时目录文件
        FileUtils.cleanDirectory(new File(tempDir));

        System.out.println("[FINISHED] total spent=" + (System.currentTimeMillis() - startTime) + "ms");
    }
}
