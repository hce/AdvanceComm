package com.iteknical.tony.acomm;

import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class Uniq {
    /**
     * 小文件分块去重输出
     * 
     * @param tempDir
     * @param splitXPrefix
     * @param outputFileName
     * @param splitSum
     * @param charset
     * @throws Exception
     */
    public static void singleThreadUniq(String tempDir, String splitXPrefix, String outputFileName, int splitSum,
        Charset charset) throws Exception {
        // System.out.println("compare start");
        // long curTimestamp = System.currentTimeMillis();

        BufferedWriter resultBufferWriter = Files.newBufferedWriter(Paths.get(outputFileName));

        for (int i = 0; i < splitSum; ++i) {
            Set<String> setA = new HashSet<String>();
            Files
                .lines(Paths.get(new StringBuilder(tempDir).append("/").append(splitXPrefix).append(i).toString()),
                    charset)
                .forEach(s -> setA.add(s));

            for (String s : setA) {
                resultBufferWriter.write(s);
                resultBufferWriter.write('\n');
            }
        }

        resultBufferWriter.close();

        // System.out.println("compare spent=" + (System.currentTimeMillis() - curTimestamp) + "ms");
    }
}
