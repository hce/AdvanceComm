package com.iteknical.tony.acomm;

public class CountFileLinesBenchmark {
    private static final String FILE_A_NAME = "D:\\Dic\\crackstation_left";

    public static void main(String[] args) throws Exception {
        long startTime = 0L;

        // startTime = System.currentTimeMillis();
        // System.out.println(FileUtil.countFileLines(FILE_A_NAME, FileUtil.COUNT_FILE_LINES_METHOD_NIO_1));
        // System.out.println(System.currentTimeMillis() - startTime);

        startTime = System.currentTimeMillis();
        System.out.println(CountFileLinesUtil.countFileLines(FILE_A_NAME, CountFileLinesUtil.COUNT_FILE_LINES_METHOD_IO_1));
        System.out.println(System.currentTimeMillis() - startTime);

        // startTime = System.currentTimeMillis();
        // System.out.println(FileUtil.countFileLines(FILE_A_NAME, FileUtil.COUNT_FILE_LINES_METHOD_IO_2));
        // System.out.println(System.currentTimeMillis() - startTime);

        startTime = System.currentTimeMillis();
        System.out.println(CountFileLinesUtil.countFileLines(FILE_A_NAME, CountFileLinesUtil.COUNT_FILE_LINES_METHOD_NIO_2));
        System.out.println(System.currentTimeMillis() - startTime);
    }
}
