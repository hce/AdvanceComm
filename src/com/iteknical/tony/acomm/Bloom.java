package com.iteknical.tony.acomm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

public class Bloom {
    /** 读BUFFER大小 */
    private static final int    READ_BUFFER_SIZE   = 8 * 1024;

    /** BloomFilter Config */
    // TODO 待计算入参
    @Deprecated
    private static final long   EXPECTED_INSERTION = 12000000;
    // TODO 待计算入参
    @Deprecated
    private static final double FPP                = 0.1;

    /**
     * 2线程bloom
     * <p>
     * 同时进行a bloom b与b bloom a，得到x和y文件
     * </p>
     * 
     * @param fileAName
     * @param fileBName
     * @param fileXName a bloom b
     * @param fileYName b bloom a
     * @param charset
     * @throws Exception
     */
    public static void twoThreadBloom(String fileAName, String fileBName, String fileXName, String fileYName,
        double fpp, Charset charset) throws Exception {
        // TODO 此处bloom可以不基于String而基于byte[]，可以极大的优化效率，但是对于字符编码有没有坑未知

        final CountDownLatch latch = new CountDownLatch(2);

        Thread splitAThread = buildNewBloomThread(fileAName, fileBName, fileXName, fpp, charset, latch);
        Thread splitBThread = buildNewBloomThread(fileBName, fileAName, fileYName, fpp, charset, latch);

        splitAThread.start();
        splitBThread.start();

        latch.await();
    }

    /**
     * 
     * @param bloomFileName
     * @param compareFileName
     * @param outputFileName
     * @param charset
     * @param latch
     * @return
     */
    private static Thread buildNewBloomThread(final String bloomFileName, final String compareFileName,
        final String outputFileName, double fpp, final Charset charset, final CountDownLatch latch) {
        // TODO 待替换更合理的写法
        return new Thread(() -> {
            try {
                long expectedInsertion = CountFileLinesUtil.countFileLines(bloomFileName);

                System.out.println("bloom build start, file=" + bloomFileName);
                long startTime = System.currentTimeMillis();

                BloomFilter<String> bloomFilter = BloomFilter.create(Funnels.stringFunnel(charset),
                    expectedInsertion, fpp);

                BufferedReader splitBufferReaderA = new BufferedReader(new FileReader(bloomFileName),
                    READ_BUFFER_SIZE);
                String s;
                while ((s = splitBufferReaderA.readLine()) != null) {
                    bloomFilter.put(s);
                }
                splitBufferReaderA.close();

                System.out.println("bloom build finished, file=" + bloomFileName + ", spent="
                    + (System.currentTimeMillis() - startTime) + "ms");

                System.out
                    .println("bloom compare start, ,bloom file=" + bloomFileName + ", compre file=" + compareFileName);
                startTime = System.currentTimeMillis();

                BufferedReader splitBufferReaderB = new BufferedReader(new FileReader(compareFileName),
                    READ_BUFFER_SIZE);
                BufferedWriter splitBufferWriterB = Files.newBufferedWriter(Paths.get(outputFileName));
                while ((s = splitBufferReaderB.readLine()) != null) {
                    if (bloomFilter.mightContain(s)) {
                        splitBufferWriterB.write(s);
                        splitBufferWriterB.write('\n');
                    }
                }
                splitBufferReaderB.close();
                splitBufferWriterB.close();

                System.out.println(
                    "bloom compare finished, bloom file=" + bloomFileName + ", compare file=" + compareFileName
                        + ", spent="
                        + (System.currentTimeMillis() - startTime) + "ms");
            } catch (Exception e) {
                e.printStackTrace();
            }
            latch.countDown();
        });
    }

    /**
     * 4线程bloom
     * <p>
     * 初步测试效率不高
     * </p>
     */
    @Deprecated
    public static void fourThreadBloom(String FILE_A_NAME, String FILE_B_NAME, String FILE_X_NAME, String FILE_Y_NAME,
        Charset charset) {
        long startTime = System.currentTimeMillis();

        try {
            final CountDownLatch latch = new CountDownLatch(4);

            final BloomFilter<String> bloomFilterA = BloomFilter.create(Funnels.stringFunnel(charset),
                EXPECTED_INSERTION, FPP);
            final BloomFilter<String> bloomFilterB = BloomFilter.create(Funnels.stringFunnel(charset),
                EXPECTED_INSERTION, FPP);

            Thread splitA1Thread = buildNewBloomThread(FILE_A_NAME, 0, 5000000, bloomFilterA, latch);
            Thread splitA2Thread = buildNewBloomThread(FILE_A_NAME, 5000000, 10000000, bloomFilterA, latch);
            Thread splitB1Thread = buildNewBloomThread(FILE_B_NAME, 0, 5000000, bloomFilterB, latch);
            Thread splitB2Thread = buildNewBloomThread(FILE_B_NAME, 5000000, 10000000, bloomFilterB, latch);

            splitA1Thread.start();
            splitA2Thread.start();
            splitB1Thread.start();
            splitB2Thread.start();

            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("two thread bloom and compare spent=" + (System.currentTimeMillis() - startTime) + "ms");
    }

    @Deprecated
    private static Thread buildNewBloomThread(final String inputPathA, final int skipLine, final int endLine,
        final BloomFilter<String> bloomFilter, final CountDownLatch latch) {
        return new Thread(() -> {
            long startTime = System.currentTimeMillis();
            try {
                BufferedReader splitBufferReaderA = new BufferedReader(new FileReader(inputPathA),
                    READ_BUFFER_SIZE);
                int lineCounter = 0;
                String s;
                while ((s = splitBufferReaderA.readLine()) != null) {
                    lineCounter++;
                    if (lineCounter < skipLine) {
                        continue;
                    }
                    if (lineCounter > endLine) {
                        break;
                    }
                    bloomFilter.put(s);
                }
                splitBufferReaderA.close();

                System.out.println("thead spent=" + (System.currentTimeMillis() - startTime));
            } catch (Exception e) {
                e.printStackTrace();
            }
            latch.countDown();
        });
    }
}
